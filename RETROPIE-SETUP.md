# Setting Up A RetroPie From Scratch (An Opinionated Guide)

This guide will walk you through setting up your own RetroPie machine from start to finish. What is [RetroPie](https://retropie.org.uk/)? It is a video game emulator 
which allows you to play old-school games on devices like a [Raspberry Pi](https://www.raspberrypi.org/).    

This guide will make all efforts to enumerate all the steps and their corresponding details, however since this is a project that is 
working with both hardware and software, some technical accumen is required.    

Hardware you will need for this project (with links to purchase):
  1. [Raspberry Pi 3](http://amzn.to/2gvBQPs)
  1. [Micro SD Card (UHS speed class U1 or U3)](http://amzn.to/2g5OzU4)
  1. [USB Retro Controllers (2-pack)](http://amzn.to/2ffjteO)
  1. [Pi Case (optional, but recommended)](http://amzn.to/2eS4Xv9)
  1. [USB Male A to Micro B cable - 6ft (optional if you already have one)](http://amzn.to/2gvAIvh)
  1. [USB wall charger (optional if you already have one, min 1A output)](http://amzn.to/2fsrJWJ)
  1. [Wireless keyboard (optional, but heavily recommended. USB generic keyboard OK too)](http://amzn.to/2g8LXpI)     

Software you will need for this project:
  1. [RetroPie OS](https://retropie.org.uk/download/)
  1. Game ROMs (more details below)    

Tools you will need for this project:
  1. A PC or Mac (sorry, iPads and other tablets won't do)
  1. terminal.app (the OS X command line terminal)
  1. An SD card reader    

## A Note Before We Begin    

This guide assumes that you are using a Mac and the steps outlined will reflect that. This means that you can follow the steps in this guide 
verbatim if you are on a Mac or a Linux machine. Can you still set up a RetroPie using Windows? Yes, you absolutely can, but this guide does not 
contain instructions for that scenario. If you want to include instructions for Windows setup, feel free to add them and send a pull request.    

Do I need the keyboard? The straight answer to that question is no. If you are familiar with SSH and are comfortable configuring systems via a shell, you don't need an 
external keyboard. Do you need the wireless keyboard specifically? No, you can use a generic USB keyboard as well. I find that the wireless keyboard works directly out of the box 
with no setup (yes, you may have to configure a USB keyboard before it works correctly). Although we only use the keyboard briefly in this project, a keyboard is a required piece of 
kit when you own a Raspberry Pi. You will find that having one on hand comes in very useful when you decide to repurpose your Pi in the future (which you can easily do with only a 
second SD card loaded with a different OS image). I have set up and configured dozens of Pi's and I can tell you that using an external keyboard will make your life easier.  


## Step 1: Flash the SD Card    

All of the steps in this section happen on your computer. We do not bring the Raspberry Pi into the mix until Step 2.    

Go to the [RetroPie Download](https://retropie.org.uk/download/) page and download the correct package (if you are using a Pi 3, select the Raspberry Pi 2/3 download). 
Save this file to your desktop. 

Plug in your micro SD card into your computer. The disk will auto-mount, meaning it is visible in your Finder. Open terminal.app (you can search for it or find it in Applications > Utilities). 
If you've never used the command prompt before, don't freak out; this is your first hacker project and you'll do fine. We want to find the mount point of our SD card so we type:    

`diskutil list`    

You will be shown a list of the various disks mounted on your OS. The disk names won't say "SD Card" so we have to evaluate which one our card could be. I'm using a 32GB 
SD card so I'll look for an entry that matches that stat. *Helpful hint: unplug any other external disks before you run diskutil, this will make finding your SD card easier.* 
Here is what the entry for my SD card looks like in the diskutil output:    

```
/dev/disk2 (internal, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:     FDisk_partition_scheme                        *31.9 GB    disk2
   1:             Windows_FAT_32 NO NAME                 31.9 GB    disk2s1
```    

This disk matches the stats of our SD card; it is a physical disk (see the "internal, physical" attribute on the first line) and the size of 31.9 GB is correct (actual disk sizes are always 
smaller than what is shown on the box). So we now know that our SD card is referred to as `disk2` and it is mounted at `/dev/disk2` on our system.    

*Note! Your mount point may be different than what you see here. This guide will continue to refer to the disk as `disk2` with path `/dev/disk2` but if your disk is mounted at a different 
location on your system, make sure to adjust the commands in this guide accordingly!*

Next we need to unmount the disk so we can write our RetroPie image to it. Do this by entering this command:    

`diskutil unmountDisk /dev/disk2`    

If the command was successful, the system will echo back:    

`Unmount of all volumes on disk2 was successful`    

---
#### A Quick Note on the Command Line    
Commands that you enter on the command line need to be very exact. Commands are case-sensitive and spaces are important. Be sure to pay close attention to how you enter your commands. Don't hit ENTER until you are sure 
the command you have written is correct. If I didn't already mention this: Use this guide at your own risk.    

---    

Now that our disk has been unmounted, we can write our RetroPie image to our SD card. If you are recycling a used SD card you need to know: this will erase all files on your card! 
If your card contains precious photos of Gram Gram's 97th birthdy party, those photos will be erased. Make sure you are using a card that is OK to be wiped clean.    

First we need to unzip the image file we downloaded. You can see that it has a `.gz` file extension, which just means that it has been gzipped. We will use a utility called 
`gunzip` to extract our image. You can verify that your system has `gunzip` by typing `which gunzip` at the command prompt. If the system echoes back a path, such as `/usr/bin/gunzip` 
then you have it installed. If it echoes back nothing, you will need to find another way to unzip your file. Honestly, go Google "extract .gz file" for instructions better than 
what I can provide here. Using gunzip, extract your image with the following commands (hit ENTER after each line):    

`cd ~/Desktop`    

`gunzip retropie-4.1-rpi2_rpi3.img.gz`    

The first command just moves our prompt into the Desktop directory, since that is where we downloaded our image to (This is why I said to download the image to your Desktop. If you 
saved the file elsewhere, cd into that directory instead). The second command extracts the image. Note that you may not be able to copy and paste 
the second command if your image's filename is different than the one I am using here. A succesful extraction means that the command prompt will not echo back a response (when things go according to plan, 
the command line doesn't say anything). To check that our file was extracted, you can type `ls` and would find a file listed as `retropie-4.1-rpi2_rpi3.img` with the `.img` file extension and not `.img.gz`.    

To write our image to our card, use the following command (read explanation below first if you are uncertain):     

`sudo dd bs=4m if=retropie-4.1-rpi2_rpi3.img of=/dev/disk2`    

Starting our command with `sudo` means we are invoking higher permissions with this command. This means that you will be prompted to enter your user's password before the command is executed. This 
is just the same password you use to log in to your computer normally.    

`if=` refers to the "in file" or input, which in our case is the RetroPie image file. Likewise, `of=` is the "out file" or ouput, which is obvioulsy our SD card located at `/dev/disk2`. 
The other flag `bs=4m` refers to the byte size, or how much data will be transfered at a time. You may recieve an error which says `dd: bs: illegal numeric value`. If this is the case, change `bs=4m` to 
read `bs=4M`.     

If you enter this command and nothing happens, congratulations, it is working. Remember, when things go according to plan, the command line doesn't respond. Don't get too excited though, this 
process will take a long time to complete. This means don't go entering any other commands into the command line until the process has finished. Total time could range anywhere between 5 and 30 minutes; it all 
depends on the size of your SD card and the speed of your computer. You will know the process is finished when the system returns a blank command prompt.    

Now that our image has been written to our card, we just need to eject our disk with this command:    

`diskutil eject /dev/disk2`    

The system will respond with `Disk /dev/disk2 ejected` and it is safe to remove your SD card from your computer. Congrats, you've just flashed your OS image to disk.    

## Step 2: Boot up RetroPie    

Before we get too much further, it is a good idea to plug our Pi in and make sure our OS image was written to the card correctly. Let's get plugged in and ready, so complete these steps: 
  1. Insert the SD card into the Pi
  1. Plug in one of your controllers to any of the 4 USB ports on the Pi. 
  1. Plug in your HDMI cable to the Pi, the other end connected to a TV or monitor.
  1. Plug your external keyboard into the Pi (either the wireless dongle or the USB cord from a wired version, whichever you are using)
  1. Plug your USB micro cable into the Pi, the other end plugged into the wall charger (The Pi will turn on as soon as it is plugged in, do this step last to make sure the conroller registers correctly).

Once your Pi boots up it will briefly display a message about resizing the disk. It will then automatically restart itself. This is normal.    

Once the Pi boots the second time, you will be shown the RetroPie Boot screen. Congrats, your OS image worked. The next screen you should see will say "Gamepad Detected" and will prompt you to set up 
your controller. Follow the instructions on-screen. After you set up the buttons on your controller, the system may present additional buttons that are not found on your controller. 
Simply hold down any button (long press) to skip that setup. When you have gone through the list, you will be prompted to continue.     

After your controller is set up, you will taken to the RetroPie home screen. Hit `A` to be taken to the RetroPie settings screen. If you are using the Raspberry Pi 3 Model B, 
then your Pi has WiFi built in and you can configure your wireless connection on this screen. Scroll down to WiFi and hit `A`. This will load a blue configureation screen. You 
will notice that you cannot use your game controller to select a menu item, so this is where your external keyboard comes in handy.     

Choose option 1 - Connect to WiFi network. Find your WiFi name in the list and select it. Then enter your WiFi password when promted. After saving your selection, you will be 
brought back to the original configuration screen. Before you exit this screen, make a note of the Current IP listed at the top of the configuration box. For the purpose of 
this guide, we're going to say that our Pi has an IP address of `192.168.2.2`. We will use this address later to install our game ROMs.    

Exit the WiFi configuration screen (You can just press the ESC key on your keyboard).    

## Step 3: Installing game ROMs    

Obviously, this is the most important part; without any games, our RetroPie would be pointless. You will first need to aquire ROMs by searching for ... Look, I don't know how 
legal it is to obtain ROMs and because of this I'm not going to tell you how to do it. You're a capable person who has made it this far, I'm sure you know how to use the power of Google 
or torrents or however you might acquire your games. Just make sure you get them from a reputable source and for the sake of the rest of this guide, save your ROM files to 
your Desktop. 

We're going to be using SSH to manage our ROMs because 1. It doesn't require more software tools than what we already have and, 2. If you're looking for hacker cred, SSH is 
just cooler. If you're really over your head on this you could probably use an easier method like SFTP or even transferring your ROMs using a USB drive. I guess you could find 
a guide to those methods [here](https://github.com/retropie/retropie-setup/wiki/Transferring-Roms). If you're still with me on SSH then the first thing we need to do is configure 
our Raspberry Pi to accept SSH connections. Go the RetroPie settings menu and select RASP-CONFIG. This will bring up another blue screen config menu, so get your external keyboard handy. 
Choose "Advanced Options" and then select "SSH." You will be asked, "Would you like the SSH server enabled?" and your response will be to select "Yes." The menu will confirm your 
selection and you will be taken back to the main config menu. Hit ESC to exit. Using the game controller, go back to the 
main RetroPie menu, hit the "Start" button, choose "Quit", then choose "Restart System."    

Now go back to your computer and open terminal.app. We want to make an SSH connection to our Pi, so type:    

`ssh pi@192.168.2.2`    

Make sure the IP address is the same as the one you wrote down earlier. (If for some reason you didn't write that number down, you can go to the RetroPie settings menu and choose the option
"Show IP" to tell you what your Pi's current IP address is.) You will see a prompt that says the authenticity of the host can't be established, do you want to continue? 
Type `yes` and hit enter. You will now be prompted for the password; type `raspberry` and hit enter. You will be greeted with the RetroPie command prompt.     

Congrats, you are not inside the shell of your Pi. Type `ls` to list all of the files and directories found in your current directory (you are in the HOME dir; you will always be 
dropped into this location when logging in). You should see:    

```
pi@retropie:~ $ ls
RetroPie  RetroPie-Setup
```    

We want to move into the RetroPie directory, so type:     

`cd RetroPie`    

Repeat the process of `ls` and you will see:    

```
pi@retropie:~/RetroPie $ ls
BIOS  retropiemenu  roms  splashscreens
```

We're getting closer; we can see a directory called `roms` and as you might guess, that is where we will be saving our game ROMs. Once more, move into the `roms` directory 
by typing:    

`cd roms`    

You should be shown a large list of additional directories that looks like this:    

```
pi@retropie:~/RetroPie/roms $ ls
amstradcpc  atari2600  atarilynx  fds       gb   gbc      mame-libretro  mastersystem  msx  neogeo  ngp   pcengine  psx      segacd   snes     zxspectrum
arcade      atari7800  fba        gamegear  gba  genesis  mame-mame4all  megadrive     n64  nes     ngpc  psp       sega32x  sg-1000  vectrex
```    

These directories represent all the different types of game ROMs you can load onto RetroPie. For this example, we're going to load a few NES games, so we are interested in 
the directory named `nes`. If we were to `cd nes` and again run `ls`, the directory would be empty because we have not yet loaded any games. Let's fix that.   

Before we go any further, let's close our SSH session with our Pi. We won't need this shell any more and it may confuse you if we leave it open, so type:    

`exit`    

You will be logged out of the Pi and your command prompt will return to your local system.     

Let's assume that you downloaded a ROM to your Desktop and it was named "Super Mario Bros..zip" and you wanted to load it onto your RetroPie. First, go back 
to Terminal and type `cd ~/Desktop` to move to your desktop directory, which is where your ROM is saved. We are now going to transfer our ROM to our Pi using 
a utility called `scp`, which is shorthand for Secure Copy. Type this into the command line (explanation to follow):    

`scp Super\ Mario\ Bros..zip pi@192.168.2.2:~/RetroPie/roms/nes`    

Right after `scp` we are specifying the file we want to transfer first (it is always in the order of 'origin' 'destination'). Notice that any spaces in the filename are preceeded 
by a backslash "\" - this is what is known as an escape character and it allows us to use a filename with a space in it. Do not leave these out. If your filename has a space in it, always 
prepend that space with a backslash.     

The second part of the command is the destination that we want to copy our file to. In this case it looks very similar to our ssh connection string we used earlier. The `scp` 
utility is most commonly used to transfer files between remote systems, so our destination location must start with a reference to that remote system. We use `pi@192.168.2.2` to 
specify the user and system location, then a `:` as a separator, then we provide the destination path. We learned earlier that our NES ROMs should be placed in `/RetroPie/roms/nes` so that 
is the string we use. Notice that we place a `~` in front, making the entire path `~/RetroPie/roms/nes`. The tilde is a shortcut to our HOME directory. Don't think too much about it 
now, just make sure to use the tilde when you see it here.     

When you run this command, you will again be prompted for the password for user pi, type `raspberry` (same as before when we logged in via SSH). 
The output of the `scp` command will look something like this:    

```
$ scp Super\ Mario\ Bros..zip pi@192.168.2.64:~/RetroPie/roms/nes
pi@192.168.2.2's password: 
Super Mario Bros..zip                                                                                                       100%   31KB  30.7KB/s   00:00 
```    

This indicates to us that the file transfer was succesful.     

At this point all you need to do is restart your Pi (you can also just restart EmulationStation which is in option under Quit in the RetroPie menu). After the system restarts, 
you will find that the home screen now lists Nintendo as an option. Choosing Nintendo should show a list of all installed games, which for us will just be "Super Mario Bros.". 
Go ahead and select the game to start it and feel the nostalgia.     

To load more games just repeat the `scp` steps above that we used to load Super Mario Bros. You can load ROMs for other systems like Super NES, Atari 2600, Nintendo 64, etc. by 
placing the ROMs in their correct directories under `~/RetroPie/roms/`. Remember that you may want to use a different controller for these other systems as well, which just 
means you will need to plug it in and configure it like we did earlier.   

## Step 4: Play Games    

Seriously, what are you waiting for?     

### Restarting    

If you remember the old school Nintendo then you will recall that restarting the system never hurt it. In fact, that was how you usually ended a game. The same is true for your Pi; 
You can always restart the Pi by simply unplugging the power and then plugging it back in. Don't worry, you will not hurt the system.    

### Re-purposing You Pi    

What if you get sick of playing games? Can you use your Pi for something else? The answer is Yes!. The RetroPie OS and files are contained to the SD card only. If you want to 
use your Pi with another OS or for another purpose, just swap out the SD card.    

### Disclaimer    

The information in this guide is provided "as is" and is deemed to be correct according to the knowledge of the author at the time of publication. You realize that following this 
guide could result in harm to your equipment and as such you choose to follow this guide voluntarily and at your own risk.     
Don't acquire games illegally and do not distrubute illegal copies of games.   



